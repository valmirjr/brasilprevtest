package br.com.valmirsystems.api.exceptionhandler;

import java.time.LocalDateTime;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.valmirsystems.domain.exception.BussinessException;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler{
	
	@Autowired
	private MessageSource messageSource;
	
	@ExceptionHandler(BussinessException.class)
	public ResponseEntity<Object> handleNegocio(BussinessException ex, WebRequest request) {
		var status = HttpStatus.BAD_REQUEST;
		var problem = new Problem();
		problem.setStatus(status.value());
		problem.setTitle(ex.getMessage());
		problem.setDateTime(LocalDateTime.now());
		return handleExceptionInternal(ex, problem, new HttpHeaders(), status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		var field = new ArrayList<Problem.Field>();
		
		for(ObjectError error: ex.getBindingResult().getAllErrors()) {
			String name = ((FieldError)error).getField();
			String messagem = messageSource.getMessage(error, LocaleContextHolder.getLocale());
			field.add(new Problem.Field(name, messagem));
		}
		
		var problem = new Problem();
		problem.setStatus(status.value());
		problem.setTitle("Exists one or more invalid fields");
		problem.setDateTime(LocalDateTime.now());
		problem.setField(field);
		return super.handleExceptionInternal(ex, problem, headers, status, request);
	}
}
