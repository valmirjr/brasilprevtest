package br.com.valmirsystems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BrasilPrevTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrasilPrevTestApplication.class, args);
	}

}
