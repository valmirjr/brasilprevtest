package br.com.valmirsystems.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.valmirsystems.domain.exception.BussinessException;
import br.com.valmirsystems.domain.model.Client;
import br.com.valmirsystems.domain.repository.ClientRepository;

@Service
public class ClientService {
	
	@Autowired
	private ClientRepository clientRepository;
	
	public Client salve(Client client) {
		Client clientExist = clientRepository.findByEmail(client.getEmail());
		System.out.println(clientExist.getEmail());
		if(clientExist != null && !clientExist.equals(client)) {
			throw new BussinessException("Client e-mail already exist salved.");
		}
		return clientRepository.save(client);
	}
	
	public void delete(Long clientId) {
		clientRepository.deleteById(clientId);
	}
}
